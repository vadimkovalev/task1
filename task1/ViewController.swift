//
//  ViewController.swift
//  task1
//
//  Created by MacBookPro15 on 3.02.22.
//

import UIKit



var lang = "ru"
var greetings = "greetings".localized(lang)
var greetingsText = "greetingsText".localized(lang)

class ViewController: UIViewController {

    let pickerLanguage = ["english", "русский", "беларускi"]
    var selectedLanguage : String?
   // lazy var margins = view.layoutMarginsGuide
    lazy var margins = view.safeAreaLayoutGuide


    let titleImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo"))
        imageView.tintColor = .systemCyan
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    

    let textView: UITextView = {
       let textview = UITextView()

      
        textview.textAlignment = .center
        textview.isEditable = false
        textview.backgroundColor = .secondarySystemBackground
        textview.translatesAutoresizingMaskIntoConstraints = false
        return textview
    }()
    
    let pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        return pickerView
    }()
    
    private let buttonAutoColorScheme: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Auto", for: .normal)
       button.setTitleColor(.red, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector (initAutoScheme), for: .touchUpInside)
        return button
    }()
    @objc func initAutoScheme(){
        buttonLightColorScheme.setTitleColor(.black, for: .normal)
        buttonDarkColorScheme.setTitleColor(.black, for: .normal)
        buttonAutoColorScheme.setTitleColor(.red, for: .normal)
        view.overrideUserInterfaceStyle = .unspecified

    }
    let buttonLightColorScheme: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Light", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector (initLightScheme), for: .touchUpInside)
        return button
    }()
    @objc func initLightScheme(){
        buttonLightColorScheme.setTitleColor(.red, for: .normal)
        buttonDarkColorScheme.setTitleColor(.black, for: .normal)
        buttonAutoColorScheme.setTitleColor(.black, for: .normal)
        view.overrideUserInterfaceStyle = .light

    }
    
    let buttonDarkColorScheme: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Dark", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector (initDarkScheme), for: .touchUpInside)
        return button
    }()
    @objc func initDarkScheme(){
        buttonLightColorScheme.setTitleColor(.black, for: .normal)
        buttonDarkColorScheme.setTitleColor(.red, for: .normal)
        buttonAutoColorScheme.setTitleColor(.black, for: .normal)
        view.overrideUserInterfaceStyle = .dark
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .secondarySystemBackground

        view.addSubview(titleImageView)
        view.addSubview(textView)
        textView.attributedText = createText()
        textView.textAlignment = .center
        view.addSubview(pickerView)
        view.addSubview(buttonAutoColorScheme)
        view.addSubview(buttonLightColorScheme)
        view.addSubview(buttonDarkColorScheme)
        

        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.selectRow(1, inComponent:0, animated:true)
        setupLayout()

}


    private func setupLayout(){



        NSLayoutConstraint.activate([
            titleImageView.centerXAnchor.constraint(equalTo: margins.centerXAnchor),
            titleImageView.topAnchor.constraint(equalTo: margins.topAnchor, constant: 0),
            titleImageView.widthAnchor.constraint(equalToConstant: 150),
            titleImageView.heightAnchor.constraint(equalTo:margins.heightAnchor, multiplier: 0.38),
            
            textView.topAnchor.constraint(equalTo: titleImageView.bottomAnchor, constant: 0),
            textView.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
            textView.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
            textView.bottomAnchor.constraint(equalTo: pickerView.topAnchor),
            
            pickerView.widthAnchor.constraint(equalToConstant: 150),
            pickerView.heightAnchor.constraint(equalToConstant: 100),
            pickerView.centerXAnchor.constraint(equalTo: margins.centerXAnchor),
            pickerView.bottomAnchor.constraint(equalTo: buttonAutoColorScheme.topAnchor, constant: 0),
            
            buttonAutoColorScheme.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: 0),
            buttonAutoColorScheme.centerXAnchor.constraint(equalTo: margins.centerXAnchor),
            buttonAutoColorScheme.widthAnchor.constraint(equalToConstant: 50),
            buttonAutoColorScheme.heightAnchor.constraint(equalToConstant: 50),
            
            buttonLightColorScheme.heightAnchor.constraint(equalTo: buttonAutoColorScheme.heightAnchor),
            buttonLightColorScheme.topAnchor.constraint(equalTo: buttonAutoColorScheme.topAnchor),
            buttonLightColorScheme.rightAnchor.constraint(equalTo: buttonAutoColorScheme.leftAnchor),
            buttonLightColorScheme.widthAnchor.constraint(equalTo: buttonAutoColorScheme.widthAnchor),
            
            buttonDarkColorScheme.heightAnchor.constraint(equalTo: buttonAutoColorScheme.heightAnchor),
            buttonDarkColorScheme.topAnchor.constraint(equalTo: buttonAutoColorScheme.topAnchor),
            buttonDarkColorScheme.leftAnchor.constraint(equalTo: buttonAutoColorScheme.rightAnchor),
            buttonDarkColorScheme.widthAnchor.constraint(equalTo: buttonAutoColorScheme.widthAnchor)
            
        ])
        
    }

}

    
extension ViewController : UIPickerViewDelegate, UIPickerViewDataSource{
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerLanguage.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerLanguage[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedLanguage = pickerLanguage[row]
        switch selectedLanguage {
        case "english":
            print("en")
            lang = "Base"
            textView.attributedText = createText()
            textView.textAlignment = .center

        case "русский":
            print("ru")

            lang = "ru"
            textView.attributedText = createText()
            textView.textAlignment = .center

        case "беларускi":
        print("by")

            lang = "be-BY"
            textView.attributedText = createText()
            textView.textAlignment = .center
        default :
            print("???")
        }
    }
    
    func createText()->NSMutableAttributedString{
        let greetings = "greetings".localized(lang)
        let greetingsText = "greetingsText".localized(lang)
        let attributedText = NSMutableAttributedString(string: greetings, attributes: [.font: UIFont.boldSystemFont(ofSize: 22), .foregroundColor: UIColor.systemCyan])
        attributedText.append( NSMutableAttributedString(string: greetingsText, attributes: [.font: UIFont.systemFont(ofSize: 14), .foregroundColor: UIColor.systemGray]))
        return attributedText
    }
    
}
extension String {
func localized(_ lang:String) ->String {

    let path = Bundle.main.path(forResource: lang, ofType: "lproj")
    let bundle = Bundle(path: path!)

    return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
}
    
}


